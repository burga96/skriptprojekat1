import csv
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Projekat1.settings")
os.environ['DJANGO_SETTINGS_MODULE'] = 'Projekat1.settings'

import django
django.setup()

with open('rasporedCSV.csv', encoding='utf-8') as csvFile:
    readCSV = csv.reader(csvFile, delimiter=';')
    first = []
    for row in readCSV:
        if row:                         #add just rows that have data
            first.append(row)
    first.pop(0)                        #remove descriptive data
    first.pop(0)                        #remove descriptive data
    PredmetRow = []

    for row in first:
        if len(row) == 2 :
            PredmetRow = row
            print('----PREDMET--------')
            print(PredmetRow)
            print('----PREDMET--------')
        else:
            print('----POLJE--------')
            nastavnikPredavanja = row[1]
            sifraNastavnikaPredavanja = row[2]
            grupePredavanja = row[3]
            danPredavanje = row[5]
            casPredavanje = row[6]
            ucionicaPredavanje = row[7]

            praktikantPraktikum = row[1+8]
            sifrapraktikantPredavanja = row[2+8]
            grupePraktikum = row[3+8]
            danPraktikum = row[5+8]
            casPraktikum = row[6+8]
            ucionicaPraktikum = row[7+8]

            asistentVezbe = row[1 + 16]
            sifraAsistentaVezbe = row[2 + 16]
            grupeVezbe = row[3 + 16]
            danVezbe = row[5 + 16]
            casVezbe = row[6 + 16]
            ucionicaVezbe = row[7 + 16]

            nastavnikPredavanjeVezbe = row[1 + 24]
            sifraNastavnikaPredavanjeVezbe = row[2 + 24]
            grupePredavanjeVezbe = row[3 + 24]
            danPredavanjeVezbe = row[5 + 24]
            casPredavanjeVezbe = row[6 + 24]
            ucionicaPredavanjeVezbe = row[7 + 24]


            print('----POLJE--------')